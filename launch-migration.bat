@echo off
setlocal

:: divider to format the output
set "divider=------------------------------------------------------------------------------------------------------"

:: Get the start time, tbe used when calculcating the duration
set "startTime=%time%"

:: echos the start of the scrpt, notif!
echo %divider%
echo %date% %time% :: Script execution started.
echo %divider%

:: Include the variables from a config file
call config.bat

:: Loop through each subdirectory and find matching batch scripts
:: scripts follow this naming rule .\Jobs\J1_[TableName]\j1_[TableName]_run.bat
for /r "%scriptsDir%" %%f in (J*_run.bat) do (

    if exist "%%f" (
        echo %divider%
        echo Calling "%%~nxf" ...
        call "%%f" --context_param target_db_server=%target_db_server% --context_param target_db_login=%target_db_login% --context_param target_db_password=%target_db_password% --context_param target_db_database=%target_db_database% --context_param target_db_additionalParams=%target_db_additionalParams% --context_param target_db_port=%target_db_port% --context_param source_db_server=%source_db_server% --context_param source_db_login=%source_db_login%  --context_param source_db_password=%source_db_password%  --context_param source_db_database=%source_db_database% --context_param source_db_port=%source_db_port% --context_param source_db_additionalParams=%source_db_additionalParams%

        if errorlevel 1 (
            echo Error occurred while running %%~nxf.
            exit /b 1
        )  
        echo %%~nxf completed successfully.
        echo %divider%
    ) else (
        echo "%%f" not found.
        echo %divider%
        exit /b 1
    )
    timeout /t 15 > nul
)


echo %divider%
echo %date% %time% :: Script execution completed.
echo %date% %time% :: Elapsed time: --:--:--
echo %divider%

endlocal