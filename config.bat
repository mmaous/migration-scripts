:: Context Variables
set "target_db_server="
set "target_db_login="
set "target_db_password="
set "target_db_database="
set "target_db_port="
set "target_db_additionalParams="

set "source_db_server="
set "source_db_login="
set "source_db_password="
set "source_db_database="
set "source_db_port="
set "source_db_additionalParams="

:: holds the jobs built and exported from talend
set "scriptsDir=./jobs"
