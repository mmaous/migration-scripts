# Batch Script for Running Talend Jobs

> This repository contains a batch script (`launch-migration.bat`) that facilitates the execution of Talend jobs stored in a specific folder structure. The script takes care of running the jobs in the intended order and utilizes a common configuration file (`config.bat`) to set context variables.

## Running the Script

Follow these steps to execute the `launch-migration.bat` script and run the Talend jobs:

1. Open a command prompt or terminal window.
2. Navigate to the root directory of the repository.
3. Run the following command:

```ps1
launch-migration.bat
```

4. The script will sequentially execute the Talend jobs located in the jobs/ directory in the order specified by the folder names.
5. The script will utilize the context variables defined in config.bat for database connection and other configurations.
6. The progress and results of each executed job will be displayed in the command prompt.

## File Structure

The repository should have the following file structure:

```shell
root/
├── .gitignore
├── README.md
├── launch-migration.bat
├── config.bat
├── jobs/
│   ├── J1_[Table1]/
│   │   ├── j1_[Table1]_run.bat
│   │   └── ... (other files for Table1 job)
│   ├── J2_[Table2]/
│   │   ├── j2_[Table2]_run.bat
│   │   └── ... (other files for Table2 job)
│   └── ...
├── jobInfo.properties
└── lib/
    ├── common_dependency1.jar
    ├── common_dependency2.jar
    └── ...
```

- `launch-migration.bat`: The main batch script responsible for running the Talend jobs.
- `config.bat`: Configuration file containing context variables used by the jobs.
- `jobs/`: Folder containing subdirectories for each Talend job. Each job folder should contain a job-specific batch \* script (jX_TableX_run.bat) and other job-related files.
- `lib/`: Folder containing common dependencies used across the Talend jobs.

## NOTE :

Make sure you have the necessary permissions to execute batch scripts on your system.

The script will exit immediately if any job encounters an error during execution.
